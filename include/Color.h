#ifndef COLOR_H
#define COLOR_H

#include <allegro5/allegro.h>

class Color {
private:
    float r, g, b, a;

public:
    Color(const Color& other) = default;
    Color& operator=(const Color& other) = default;
    ~Color() = default;
    Color(const float& _r = 0.0f, const float& _g = 0.0f, const float& _b = 0.0f, const float& _a = 0.0f) : r{ _r }, g{ _g }, b{ _b }, a{ _a } {};
    Color(const ALLEGRO_COLOR& col) : r{ col.r }, g{ col.g }, b{ col.b }, a{ col.a } {};
    const ALLEGRO_COLOR ToAllegroColor() const { return ALLEGRO_COLOR{ r, g, b, a }; }
};
#endif