#include "Smelty.h"

Smelty::Smelty()
    : queue{ std::make_shared<EventQueue>() } {
    RegisterEventSources();
    SetCallbacks();
};

void Smelty::PreGameLoopSandbox() {
    std::vector<Rect2D> boxes = {
        { 0, 0, 95, 83 },
        { 95, 0, 95, 83 },
        { 190, 0, 95, 83 },
        { 0, 83, 95, 83 },
    };
    BitmapPtr b = std::make_shared<Bitmap>("res/310000105_magic_atk.png");
    film = AnimationFilm{ "id", b, boxes };
    frameNo = 0;

    currentTime = al_get_time();
    lastFrameTime = currentTime;
    lastMillisPerFrameTime = currentTime;
    millisPerFrame = 0.0;
    framesPerSecond = 0.0;

    frameCounter = 0;

    AnimationFilmHolder::SetParseEntry(
        [this](const int& cursorPos, const std::string& text) -> std::tuple<std::string, std::string, std::vector<Rect2D>, int> {
            return ParseEntryCallback(cursorPos, text);
        });

    box.SetColor({ 1.0, 0.0, 0.0, 1.0 });
    box2.SetColor({ 1.0, 1.0, 0.0, 1.0 });
    circle.SetColor({ 0.0, 1.0, 0.0, 1.0 });
    polygon.SetColor({ 0.0, 0.0, 1.0, 0.5 });

    CollisionChecker::Action action = [this](Sprite* s1, Sprite* s2) {
        s1->SetColor({ 1.0, 1.0, 1.0, 1.0 });
        s2->SetColor({ 1.0, 1.0, 1.0, 1.0 });
    };
    CollisionChecker::Get().Register(&box, &box2, action);
    CollisionChecker::Get().Register(&box, &circle, action);
    CollisionChecker::Get().Register(&box, &polygon, action);
    CollisionChecker::Get().Register(&circle, &polygon, action);

    lastTime = currentTime;
    animTimer.Start();
    fpsTimer.Start();
    keyboardTimer.Start();
}

void Smelty::Run() {
    PreGameLoopSandbox();
    GetTimer().Start();
    GetGameLoop().Loop();
}

void Smelty::EventDispatchCallback() {
    //event = queue->WaitEvent();
    bool notEmpty;
    std::tie(event, notEmpty) = queue->GetEvent();
    if (notEmpty == false) return;
    if (event.type == ALLEGRO_EVENT_TIMER) {
        if (event.timer.source == GetTimer().Get()) {
            redraw = true;
        } else if (event.timer.source == animTimer.Get()) {
            frameNo++;
            if (frameNo >= film.GetTotalFrames()) {
                frameNo = 0;
            }
            redraw = true;
        } else if (event.timer.source == fpsTimer.Get()) {
            currentTime = al_get_time();
            double elapsedTime = currentTime - lastTime;
            lastTime = currentTime;
            assert(elapsedTime > 0);
            assert(frameCounter > 0);

            millisPerFrame = (1000 * fpsTimer.GetSpeed()) / static_cast<double>(frameCounter);
            framesPerSecond = static_cast<double>(frameCounter) / elapsedTime;
            frameCounter = 0;
            redraw = true;
        } else if (event.timer.source == keyboardTimer.Get()) {
            if (keyboard.at(ALLEGRO_KEY_UP))
                boxY--;
            if (keyboard.at(ALLEGRO_KEY_DOWN))
                boxY++;
            if (keyboard.at(ALLEGRO_KEY_LEFT))
                boxX--;
            if (keyboard.at(ALLEGRO_KEY_RIGHT))
                boxX++;

            if (keyboard.at(ALLEGRO_KEY_ESCAPE))
                finished = true;

            for (int i = 0; i < ALLEGRO_KEY_MAX; i++)
                keyboard.at(i) &= KEY_SEEN;

            redraw = true;

        } else {
            assert(false);
        }

    } else if (event.type == ALLEGRO_EVENT_KEY_DOWN) {
        keyboard.at(event.keyboard.keycode) = KEY_SEEN | KEY_RELEASED;
    } else if (event.type == ALLEGRO_EVENT_KEY_UP) {
        keyboard.at(event.keyboard.keycode) &= KEY_RELEASED;
    } else if (event.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
        finished = true;
    }
}
void Smelty::InputCallback() {
    box.SetPosition(boxX, boxY);
}
void Smelty::CollisionsCallback() {
    box.SetColor({ 1.0, 0.0, 0.0, 1.0 });
    box2.SetColor({ 1.0, 1.0, 0.0, 1.0 });
    circle.SetColor({ 0.0, 1.0, 0.0, 1.0 });
    polygon.SetColor({ 0.0, 0.0, 1.0, 0.5 });
    CollisionChecker::GetConst().Check();
}
void Smelty::RenderCallback() {
    if (redraw && queue->IsEmpty()) {
        renderer.Clear();
        film.DisplayFrame({ 400, 300 }, frameNo);
        renderer.Text(GetFont().GetHandle(), 0, 0, "Hello world!");
        std::string msString = std::to_string(millisPerFrame) + " ms/frame";
        std::string fpsString = std::to_string(framesPerSecond) + " fps";
        renderer.Text(GetFont().GetHandle(), renderer.Width() - 200, 10, msString.c_str());
        renderer.Text(GetFont().GetHandle(), renderer.Width() - 200, 50, fpsString.c_str());
        renderer.Text(GetFont().GetHandle(), 30, renderer.Height() - 30, std::to_string(fpsTimer.GetSpeed()).c_str());
        renderer.Text(GetFont().GetHandle(), 30, renderer.Height() - 20, std::to_string(frameCounter).c_str());
        box.RenderBoundingArea();
        box2.RenderBoundingArea();
        circle.RenderBoundingArea();
        //polygon.RenderBoundingArea();
        renderer.Flip();
        redraw = false;
    }
}
void Smelty::FPSCallback() {
    frameCounter++;
}

bool Smelty::FinishedCallback() { return finished; }

void Smelty::RegisterEventSources() {
    renderer.RegisterQueue(queue);
    input.RegisterQueue(queue);
    timer.RegisterQueue(queue);
    animTimer.RegisterQueue(queue);
    fpsTimer.RegisterQueue(queue);
    keyboardTimer.RegisterQueue(queue);
}

void Smelty::SetCallbacks() {
    GetGameLoop().SetEventDispatch([this]() -> void { EventDispatchCallback(); });
    GetGameLoop().SetInput([this]() -> void { InputCallback(); });
    GetGameLoop().SetCollisions([this]() -> void { CollisionsCallback(); });
    GetGameLoop().SetRender([this]() -> void { RenderCallback(); });
    GetGameLoop().SetFPS([this]() -> void { FPSCallback(); });
    GetGameLoop().SetFinished([this]() -> bool { return FinishedCallback(); });
}

auto Smelty::ParseEntryCallback(const int& cursorPos, const std::string& text) -> std::tuple<std::string, std::string, std::vector<Rect2D>, int> {
    int c = cursorPos;
    std::string t = text;
    return { "", "", { { 0, 0, 0, 0 } }, -1 };
}