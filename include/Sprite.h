#ifndef SPRITE_H
#define SPRITE_H

#include "BoundingArea.h"

#include "Color.h"
#include "GeomUtils.h"

#include <list>

class Sprite {
private:
    short boundingType;
    Color color;
    BoundingArea* boundingArea;

    void RenderBoundingBox() const;
    void RenderBoundingCircle() const;
    void RenderBoundingPolygon() const;

    void SetPositionBox(const int& x, const int& y);

    void SetPositionCircle(const int& x, const int& y);

    void SetPositionPolygon(const int& x, const int& y);

public:
    Sprite(const int& x, const int& y, const int& w, const int& h);
    Sprite(const int& x, const int& y, const float& r);
    Sprite(const std::list<Point2D>& points);
    void SetColor(const Color& val) { color = val; }
    void SetPosition(const int& x, const int& y);
    void RenderBoundingArea() const;
    bool CollisionCheck(const Sprite* s) const;
    const BoundingArea& GetBoundingArea() const { return *boundingArea; }
};

#endif
