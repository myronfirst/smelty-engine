#include "EventQueue.h"

ALLEGRO_EVENT EventQueue::WaitEvent() {
    ALLEGRO_EVENT event;
    al_wait_for_event(queue.get(), &event);
    return event;
}

std::tuple<ALLEGRO_EVENT, bool> EventQueue::GetEvent() {
    ALLEGRO_EVENT event;
    bool ret = al_get_next_event(queue.get(), &event);
    return { event, ret };
}
