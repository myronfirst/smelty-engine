#ifndef BOUNDING_POLYGON_H
#define BOUNDING_POLYGON_H

#include "BoundingArea.h"

#include <list>
#include "GeomUtils.h"

class BoundingPolygon : public BoundingArea {
public:
    using PointList = std::list<Point2D>;

protected:
    PointList points;

public:
    BoundingPolygon(const PointList& _points) : points{ _points } {}
    bool Intersects(const BoundingBox& _box) const override;
    bool Intersects(const BoundingCircle& _circle) const override;
    bool Intersects(const BoundingPolygon& _poly) const override;

    bool In(int x, int y) const override;
    bool Intersects(const BoundingArea& area) const override { return area.Intersects(*this); };

    PointList GetPoints() const { return points; }
    void SetPos(const int& x, const int& y);
};

#endif
