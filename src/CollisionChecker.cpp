#include "CollisionChecker.h"

CollisionChecker CollisionChecker::singleton;

void CollisionChecker::Register(Sprite* s1, Sprite* s2, const Action& f) {
    entries.push_back({ s1, s2, f });
}

void CollisionChecker::Remove(Sprite* s1, Sprite* s2) {
    auto it = std::find_if(
        entries.begin(),
        entries.end(),
        [&s1, &s2](const Entry& e) {
            const auto es1 = std::get<0>(e);
            const auto es2 = std::get<1>(e);
            return (es1 == s1 && es2 == s2) || (es1 == s2 && es2 == s1);
        });
    entries.erase(it);
}
void CollisionChecker::Check(void) const {
    for (auto& [s1, s2, action] : entries)
        if (s1->CollisionCheck(s2))
            action(s1, s2);
}