#ifndef GAME_LOOP_H
#define GAME_LOOP_H

#include <functional>

#include <allegro5/allegro5.h>
#include <allegro5/allegro_font.h>

class GameLoop {
public:
    using Action = std::function<void(void)>;
    using Pred = std::function<bool(void)>;

private:
    Action eventDispatch, render, animations, input, ai, physics, destructions, collisions, userCode, fps;
    Pred finished;

    void Invoke(const Action& f) {
        if (f) f();
    }

public:
    GameLoop();
    virtual ~GameLoop();

    //Setters
    void SetEventDispatch(const Action& f) { eventDispatch = f; }
    void SetRender(const Action& f) { render = f; }
    void SetAnimations(const Action& f) { animations = f; }
    void SetInput(const Action& f) { input = f; }
    void SetAI(const Action& f) { ai = f; }
    void SetPhysics(const Action& f) { physics = f; }
    void SetDestructions(const Action& f) { destructions = f; }
    void SetCollisions(const Action& f) { collisions = f; }
    void SetUserCode(const Action& f) { userCode = f; }
    void SetFPS(const Action& f) { fps = f; }
    void SetFinished(const Pred& f) { finished = f; }

    //Game Loop Methods
    void EventDispatch(void) { Invoke(eventDispatch); }
    void Render(void) { Invoke(render); }
    void ProgressAnimations(void) { Invoke(animations); }
    void Input(void) { Invoke(input); }
    void AI(void) { Invoke(ai); }
    void Physics(void) { Invoke(physics); }
    void CollisionChecking(void) { Invoke(collisions); }
    void CommitDestructions(void) { Invoke(destructions); }
    void UserCode(void) { Invoke(userCode); }
    void FPS(void) { Invoke(fps); }
    bool IsFinished(void) const { return finished(); }
    void Loop(void);
    void LoopIteration(void);
};

#endif
