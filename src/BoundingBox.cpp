#include "BoundingBox.h"

#include "BoundingCircle.h"
#include "BoundingPolygon.h"

bool BoundingBox::Intersects(const BoundingBox& _box) const {
    return !(_box.GetBR().x < GetTL().x ||
             GetBR().x < _box.GetTL().x ||
             _box.GetBR().y < GetTL().y ||
             GetBR().y < _box.GetTL().y);
}
bool BoundingBox::Intersects(const BoundingCircle& _circle) const {
    return _circle.Intersects(*this);
}
bool BoundingBox::Intersects(const BoundingPolygon& _poly) const {
    BoundingPolygon selfPoly({ { GetTL().x, GetTL().y }, { GetBR().x, GetBR().y } });
    return _poly.Intersects(selfPoly);
}

bool BoundingBox::In(int x, int y) const {
    return (GetTL().x <= x && x <= GetBR().x &&
            GetTL().y <= y && y <= GetBR().y);
}

void BoundingBox::SetPos(const int& x, const int& y) {
    const float fx = static_cast<float>(x);
    const float fy = static_cast<float>(y);
    Rect2D b{ box };
    box.topLeft = { fx, fy };
    box.bottomRight = { fx + b.w(), fy + b.h() };
}