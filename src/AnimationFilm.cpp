#include "AnimationFilm.h"

#include <cassert>

auto AnimationFilm::GetFrameBox(const byte& frameNo) const -> const Rect2D& {
    assert(boxes.size() > frameNo);
    return boxes.at(frameNo);
}

void AnimationFilm::SetBitmap(const BitmapPtr& val) {
    assert(val);
    bitmap = val;
}

void AnimationFilm::PushBack(const Rect2D& r) { boxes.push_back(r); }

void AnimationFilm::DisplayFrame(const Point2D& at, const byte& frameNo) {
    const Rect2D box = GetFrameBox(frameNo);
    al_draw_bitmap_region(bitmap->Get(), box.x(), box.y(), box.w(), box.h(), at.x, at.y, 0);
}
void AnimationFilm::DisplayFrame(const Point2D& at, const byte& frameNo, BitmapPtr dest) {
    ALLEGRO_BITMAP* prevTarget = al_get_target_bitmap();
    al_set_target_bitmap(dest->Get());
    DisplayFrame(at, frameNo);
    al_set_target_bitmap(prevTarget);
}
