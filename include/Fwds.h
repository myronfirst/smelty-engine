#ifndef FWDS_H
#define FWDS_H

#include <memory>

class EventQueue;
class Bitmap;
class AnimationFilm;

using EventQueuePtr = std::shared_ptr<EventQueue>;
using BitmapPtr = std::shared_ptr<Bitmap>;
using AnimationFilmPtr = std::shared_ptr<AnimationFilm>;

using EventQueueUPtr = std::unique_ptr<EventQueue>;
using BitmapUPtr = std::unique_ptr<Bitmap>;
using AnimationFilmUPtr = std::unique_ptr<AnimationFilm>;

#endif
