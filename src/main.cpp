/*
#include "AnimationDataHolder.h"
#include "AnimationFilmHolder.h"
#include "BitmapLoader.h"
#include "CollisionHandlers.h"
#include "Fighter.h"
#include "FrameListAnimator.h"
#include "FrameRangeAnimator.h"
#include "InputController.h"
#include "MovingAnimator.h"
#include "MovingPathAnimator.h"
#include "Sprite.h"
#include "StateTransitions.h"
#include "TickAnimator.h"
#include "Wrappers.h"

#include <iostream>
#include <sstream>

InputController::Actions Player1Keybindings = {
	// Common moves
	{{"ALLEGRO_KEY_ALT"}, "block"},
	{{"ALLEGRO_KEY_V"}, "high_kick"},
	{{"ALLEGRO_KEY_B"}, "high_punch"},
	{{"ALLEGRO_KEY_V", "ALLEGRO_KEY_W"}, "upper.high_kick"},
	{{"ALLEGRO_KEY_SPACE", "ALLEGRO_KEY_V", "ALLEGRO_KEY_W"}, "jump.upper.high_kick"},
	{{"ALLEGRO_KEY_SPACE"}, "jump"},
	{{"ALLEGRO_KEY_S"}, "duck"},
	{{"ALLEGRO_KEY_D"}, "right"},
	{{"ALLEGRO_KEY_A"}, "left"},
	{{"ALLEGRO_KEY_B", "ALLEGRO_KEY_S"}, "duck.high_punch"},
	{{"ALLEGRO_KEY_D", "ALLEGRO_KEY_S"}, "duck.right"},
	{{"ALLEGRO_KEY_D", "ALLEGRO_KEY_S", "ALLEGRO_KEY_V"}, "duck.high_kick.right"},
	{{"ALLEGRO_KEY_D", "ALLEGRO_KEY_S", "ALLEGRO_KEY_V", "ALLEGRO_KEY_W"}, "duck.high_kick.right.upper"},
	{{"ALLEGRO_KEY_W"}, "upper"},
	{{"ALLEGRO_KEY_V", "ALLEGRO_KEY_W"}, "high_kick.upper"},
	// {{"ALLEGRO_KEY_ALT", "ALLEGRO_KEY_S"}, "block.duck"},
	// {{"ALLEGRO_KEY_D", "ALLEGRO_KEY_LCTRL"}, "left.low_kick"},
	// {{"ALLEGRO_KEY_D", "ALLEGRO_KEY_LCTRL"}, "left.high_kick"},

	// Rayden special Moves
	// {{"ALLEGRO_KEY_D", "ALLEGRO_KEY_LCTRL", "ALLEGRO_KEY_W"}, "duck.low_punch.right."},
	// {{"ALLEGRO_KEY_A", "ALLEGRO_KEY_LCTRL", "ALLEGRO_KEY_W"}, "duck.jump"}
	//TODO: add torpedo move (Back,Back,Forward)
};

InputController::Actions Player2Keybindings = {
	{{"ALLEGRO_KEY_PAD_1"}, "block"},
	{{"ALLEGRO_KEY_PAD_2"}, "high_kick"},
	{{"ALLEGRO_KEY_PAD_3"}, "high_punch"},
	{{"ALLEGRO_KEY_PAD_2", "ALLEGRO_KEY_UP"}, "upper.high_kick"},
	{{"ALLEGRO_KEY_PAD_0", "ALLEGRO_KEY_PAD_2", "ALLEGRO_KEY_UP"}, "jump.upper.high_kick"},
	{{"ALLEGRO_KEY_PAD_0"}, "jump"},
	{{"ALLEGRO_KEY_DOWN"}, "duck"},
	{{"ALLEGRO_KEY_RIGHT"}, "right"},
	{{"ALLEGRO_KEY_LEFT"}, "left"},
	{{"ALLEGRO_KEY_PAD_3", "ALLEGRO_KEY_DOWN"}, "duck.high_punch"},
	{{"ALLEGRO_KEY_LEFT", "ALLEGRO_KEY_DOWN"}, "duck.right"},
	{{"ALLEGRO_KEY_LEFT", "ALLEGRO_KEY_DOWN", "ALLEGRO_KEY_PAD_2"}, "duck.high_kick.right"},
	{{"ALLEGRO_KEY_LEFT", "ALLEGRO_KEY_DOWN", "ALLEGRO_KEY_PAD_2", "ALLEGRO_KEY_UP"}, "duck.high_kick.right.upper"},
	{{"ALLEGRO_KEY_UP"}, "upper"},
	{{"ALLEGRO_KEY_PAD_2", "ALLEGRO_KEY_UP"}, "high_kick.upper"},
};

void DisplayCheckVictory(ALLEGRO_FONT* font, Fighter* player, Fighter* enemy);
bool PauseGame(ALLEGRO_KEYBOARD_STATE* keyboard_state, Fighter* player, Fighter* enemy);
int main() {
	//Allegro lib inits
	wrap_init();
	wrap_init_image_addon();
	wrap_install_keyboard();
	wrap_init_font_addon();
	wrap_init_ttf_addon();
	wrap_init_primitives_addon();

	//Allegro flags, display and keyboard inits
	wrap_set_new_display_flags(ALLEGRO_WINDOWED);									  //wrong binary operator??
	wrap_set_new_display_option(ALLEGRO_VSYNC, 1, ALLEGRO_SUGGEST);					  //depends on graphic card settings
	wrap_set_new_display_option(ALLEGRO_AUTO_CONVERT_BITMAPS, 1, ALLEGRO_SUGGEST);	//depends on bitmap flag ALLEGRO_CONVERT_BITMAP which is 1 by default
	wrap_set_new_bitmap_flags(ALLEGRO_CONVERT_BITMAP);								  //auto Converts bitmap to video bitmap when display is created
	ALLEGRO_DISPLAY* screen = wrap_create_display(static_cast<int>(SCREEN_WIDTH), static_cast<int>(SCREEN_HEIGHT));
	ALLEGRO_KEYBOARD_STATE keyboard_state;
	// Bitmap backbuffer = BitmapAPI::GetScreen();
	ALLEGRO_COLOR blackColor = wrap_map_rgb(0, 0, 0);
	ALLEGRO_FONT* font = wrap_load_ttf_font("./assets/Fonts/Courier New.ttf", 16, 0);

	BitmapLoader::Initialize();
	AnimationFilmHolder::Initialize();
	AnimationFilmHolder::Load("./assets/Raiden/Raiden.ini");	//Bitmap Loading (video bitmaps)
	// Background assets
	Bitmap Background = BitmapLoader::Load("./assets/Backgrounds/csd.jpg");
	Point backgroundDisplayLocation(0, 0);
	Rect BackgroundRect(backgroundDisplayLocation, static_cast<unsigned>(SCREEN_WIDTH), static_cast<unsigned>(SCREEN_HEIGHT));

	// Set up animation data
	AnimationDataHolder::Load("./assets/Raiden/Raiden_AnimationData.ini");
	//Animators Construction

	//Initialize Player 1 //
	//mem leak? who frees new Sprite?
	Fighter Player1 = Fighter(new Sprite(300, SCREEN_HEIGHT - 200, AnimationFilmHolder::GetFilm("raiden.idle")), "ready", "raiden", FACE_RIGHT, &Player1Keybindings);
	//Initialize Player 2 //
	//mem leak? who frees new Sprite?
	Fighter Player2 = Fighter(new Sprite(500, SCREEN_HEIGHT - 200, AnimationFilmHolder::GetFilm("raiden.idle")), "ready", "raiden", FACE_LEFT, &Player2Keybindings);
	// Player2.SetHealth(-5);
	// End of Player 2 //

	wrap_clear_to_color(blackColor);
	wrap_flip_display();
	bool doExit = false;
	bool reDraw = true;
	while (!doExit) {
		//Input
		wrap_get_keyboard_state(&keyboard_state);
		if (wrap_key_down(&keyboard_state, ALLEGRO_KEY_ESCAPE)) {
			doExit = true;
		} else if (wrap_key_down(&keyboard_state, ALLEGRO_KEY_P)) {
			doExit = PauseGame(&keyboard_state, &Player1, &Player2);
		}
		Player1.GetInputController()->Handle();
		Player2.GetInputController()->Handle();

		//Animations
		unsigned long currTime = GetGameTime();
		Player1.ProgressAnimators(currTime);
		Player2.ProgressAnimators(currTime);

		//Collision
		if (Player1.GetSprite()->CollisionCheck(Player2.GetSprite())) {
			FighterToFighterCollisionHandler(&Player1, &Player2);
		}
		// Player2.GetSprite()->CollisionCheck(Player1.GetSprite());
		//Rendering
		if (reDraw) {
			reDraw = true;	//false
			wrap_clear_to_color(blackColor);
			BitmapAPI::Blit(Background, BackgroundRect, backgroundDisplayLocation);
			Player1.GetSprite()->Display(BitmapAPI::GetTargetBitmap(), FACE_RIGHT);
			Player2.GetSprite()->Display(BitmapAPI::GetTargetBitmap(), FACE_LEFT);
			Player1.DisplayHealth(font, Point(30, 550));
			Player2.DisplayHealth(font, Point(600, 550));
			Player1.DisplayFrameBox();
			Player2.DisplayFrameBox();
			DisplayCheckVictory(font, &Player1, &Player2);
			wrap_flip_display();
		}
	}

	//Free up resources
	AnimationDataHolder::CleanUp();
	AnimationFilmHolder::CleanUp();
	BitmapLoader::CleanUp();
	wrap_destroy_font(font);
	wrap_destroy_display(screen);
	return 0;
}

void DisplayCheckVictory(ALLEGRO_FONT* font, Fighter* player, Fighter* enemy) {
	ALLEGRO_COLOR color = wrap_map_rgb(0, 0, 0);
	Point pos(350, 50);
	string victoryText("");
	if (player->GetHealth() <= 0)
		victoryText = enemy->GetName() + " WINS!";
	else if (enemy->GetHealth() <= 0)
		victoryText = player->GetName() + " WINS!";
	wrap_draw_text(font, color, pos.GetX(), pos.GetY(), ALLEGRO_ALIGN_LEFT, victoryText.c_str());
}

bool PauseGame(ALLEGRO_KEYBOARD_STATE* keyboard_state, Fighter* player, Fighter* enemy) {
	unsigned long pauseTime = 0;
	unsigned long resumeTime = 0;
	pauseTime = player->StopAnimators();
	enemy->StopAnimators();
	bool doResume = false;
	bool doExit = false;
	while (!doResume) {
		wrap_get_keyboard_state(keyboard_state);
		if (wrap_key_down(keyboard_state, ALLEGRO_KEY_ESCAPE)) {
			doExit = true;
			doResume = true;
		} else if (wrap_key_down(keyboard_state, ALLEGRO_KEY_P)) {
			doResume = true;
		}
	}
	resumeTime = GetGameTime();
	assert(resumeTime > pauseTime);
	player->TimeShiftAnimators(resumeTime - pauseTime);
	enemy->TimeShiftAnimators(resumeTime - pauseTime);
	return doExit;
}
 */
#include <allegro5/allegro5.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>

#include <iostream>

#include "Animation.h"

#include "Smelty.h"

void LibraryInits() {
    al_init();
    al_install_keyboard();
    assert(al_init_image_addon());
    assert(al_init_primitives_addon());
    al_set_new_bitmap_format(ALLEGRO_PIXEL_FORMAT_ANY_32_WITH_ALPHA);
}

void LibraryShuts() {
    al_shutdown_image_addon();
    al_shutdown_primitives_addon();
}

int main() {
    LibraryInits();
    Smelty smelty;
    smelty.Run();
    std::cout << "Hello" << std::endl;
    LibraryShuts();
    return 0;
}