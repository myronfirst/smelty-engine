#include "GeomUtils.h"

Point2D Point2D::operator+() const {
    return *this;
}
Point2D Point2D::operator-() const {
    return { -x, -y };
}

Point2D& Point2D::operator+=(const Point2D& other) {
    x += other.x;
    y += other.y;
    return *this;
}

Point2D& Point2D::operator-=(const Point2D& other) {
    x -= other.x;
    y -= other.y;
    return *this;
}

Point2D Point2D::operator+(const Point2D& other) const {
    return { x + other.x, y + other.y };
}
Point2D Point2D::operator-(const Point2D& other) const {
    return *this + (-other);
}


