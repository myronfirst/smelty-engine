#include "GameLoop.h"

GameLoop::GameLoop(void) {}

GameLoop::~GameLoop(void) {}

void GameLoop::Loop(void) {
    // bool b = IsFinished();
    while (!IsFinished())
        LoopIteration();
}

void GameLoop::LoopIteration(void) {
    FPS();
    EventDispatch();
    Input();
    ProgressAnimations();
    CollisionChecking();
    UserCode();
    Render();
}