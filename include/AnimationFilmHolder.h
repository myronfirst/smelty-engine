#ifndef ANIMATION_FILM_HOLDER_H
#define ANIMATION_FILM_HOLDER_H

#include <functional>
#include <map>
#include <tuple>

#include "AnimationFilm.h"
#include "BitmapLoader.h"

class AnimationFilmHolder {
public:
    using Films = std::map<std::string, AnimationFilmPtr>;
    using ParseEntry = std::function<std::tuple<std::string, std::string, std::vector<Rect2D>, int>(const int&, const std::string&)>;
    /*
    * Parser
    * Return values: id, path, rects, flag: -1=error, 0=ended gracefully, else #chars read
    * Parameters: cursorPosition, text
    */

private:
    static AnimationFilmHolder singleton;

    Films films;
    BitmapLoader bitmaps;    // only for loading of film bitmaps
    ParseEntry parseEntry;

    auto GetFilm(const std::string& id) -> AnimationFilmPtr const;

public:
    AnimationFilmHolder(void) {}
    ~AnimationFilmHolder() { CleanUp(); }
    static auto Get(void) -> const AnimationFilmHolder& { return singleton; }
    static void SetParseEntry(const ParseEntry& val) { singleton.parseEntry = val; }
    void LoadAll(const std::string& text);
    void CleanUp(void);
};
#endif
