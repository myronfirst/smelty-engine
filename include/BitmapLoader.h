#ifndef BITMAP_LOADER_H
#define BITMAP_LOADER_H

#include <map>

#include <string>
#include "Bitmap.h"
#include "Fwds.h"

class BitmapLoader {
public:
    using Bitmaps = std::map<std::string, BitmapPtr>;

private:
    Bitmaps bitmaps;
    BitmapPtr GetBitmap(const std::string& path) const;

public:
    BitmapLoader(void) {}
    ~BitmapLoader() { CleanUp(); }
    BitmapPtr Load(const std::string& path);
    void CleanUp(void);
};

#endif
