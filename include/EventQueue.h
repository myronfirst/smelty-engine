#ifndef EVENT_QUEUE_H
#define EVENT_QUEUE_H

#include "AllegroFwds.h"

#include <tuple>

class EventQueue {
private:
    ALLEGRO_EVENT_QUEUE_UPTR queue;

public:
    EventQueue() : queue{ al_create_event_queue(), al_destroy_event_queue } {};
    void RegisterSource(ALLEGRO_EVENT_SOURCE* source) { al_register_event_source(queue.get(), source); };
    void UnregisterSource(ALLEGRO_EVENT_SOURCE* source) { al_unregister_event_source(queue.get(), source); };
    bool IsEmpty() const { return al_is_event_queue_empty(queue.get()); }
    ALLEGRO_EVENT WaitEvent();
    std::tuple<ALLEGRO_EVENT, bool> GetEvent();
};

#endif
