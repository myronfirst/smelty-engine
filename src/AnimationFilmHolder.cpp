#include "AnimationFilmHolder.h"

AnimationFilmHolder AnimationFilmHolder::singleton;

auto AnimationFilmHolder::GetFilm(const std::string& id) -> AnimationFilmPtr const {
    auto it = films.find(id);
    return (it != std::cend(films) ? it->second : nullptr);
}

void AnimationFilmHolder::LoadAll(const std::string& text) {
    int cursorPos = 0;
    while (true) {
        assert(parseEntry);
        auto [id, path, rects, chars] = parseEntry(cursorPos, text);
        assert(chars >= 0);
        if (chars == 0) return;

        assert(!GetFilm(id));
        AnimationFilmPtr f = std::make_shared<AnimationFilm>(id, bitmaps.Load(path), rects);
        assert(f);
        films.at(id) = f;

        cursorPos += chars;
    }
}

void AnimationFilmHolder::CleanUp(void) {
    for (auto& [key, val] : films)
        val.reset();
    films.clear();
}
