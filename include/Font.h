#ifndef FONT_H
#define FONT_H

#include "AllegroFwds.h"

class Font {
private:
    ALLEGRO_FONT_UPTR font;

public:
    Font() : font{ al_create_builtin_font(), al_destroy_font } {};

    const ALLEGRO_FONT* GetHandle() const { return font.get(); }
};
#endif
