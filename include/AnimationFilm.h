#ifndef ANIMATION_FILM_H
#define ANIMATION_FILM_H

#include "Bitmap.h"
#include "Fwds.h"
#include "GeomUtils.h"

#include <cstddef>
#include <string>
#include <vector>

class AnimationFilm {
public:
    using byte = uint8_t;

private:
    std::string id;
    BitmapPtr bitmap;
    std::vector<Rect2D> boxes;

public:
    AnimationFilm(const std::string& _id) : id{ _id } {}
    AnimationFilm(const std::string& _id, BitmapPtr _bitmap, const std::vector<Rect2D>& _boxes) : id(_id), bitmap{ _bitmap }, boxes{ _boxes } {};
    size_t GetTotalFrames(void) const { return boxes.size(); }
    auto GetBitmap(void) const -> const BitmapPtr& { return bitmap; }
    auto GetId(void) const -> const std::string& { return id; }

    auto GetFrameBox(const byte& frameNo) const -> const Rect2D&;
    void SetBitmap(const BitmapPtr& b);
    void PushBack(const Rect2D& r);

    void DisplayFrame(const Point2D& at, const byte& frameNo);
    void DisplayFrame(const Point2D& at, const byte& frameNo, BitmapPtr dest);
};

#endif
