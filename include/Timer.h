#ifndef TIMER_H
#define TIMER_H

#include "AllegroFwds.h"

#include "Fwds.h"

#include "EventQueue.h"

class Timer {
private:
    ALLEGRO_TIMER_UPTR timer;
    EventQueuePtr queue;

public:
    Timer(double secs = 1.0 / 60.0) : timer{ al_create_timer(secs), al_destroy_timer }, queue{ nullptr } {}

    const ALLEGRO_TIMER* Get() const {
        return timer.get();
    }
    ALLEGRO_TIMER* Get() {
        return timer.get();
    }

    void RegisterQueue(const EventQueuePtr& q) {
        queue = q;
        queue->RegisterSource(al_get_timer_event_source(timer.get()));
    }

    const EventQueuePtr& GetQueue() const { return queue; }

    double GetSpeed() const { return al_get_timer_speed(timer.get()); }
    void SetSpeed(const double& val) { al_set_timer_speed(timer.get(), val); }

    void Start() { al_start_timer(timer.get()); }
    void Stop() { al_stop_timer(timer.get()); }
    void Resume() { al_resume_timer(timer.get()); }
};
#endif
