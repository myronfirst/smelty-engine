#ifndef BOUNDING_AREA_H
#define BOUNDING_AREA_H

class BoundingBox;
class BoundingCircle;
class BoundingPolygon;

class BoundingArea {
public:
    virtual bool Intersects(const BoundingBox& _box) const = 0;
    virtual bool Intersects(const BoundingCircle& _circle) const = 0;
    virtual bool Intersects(const BoundingPolygon& _poly) const = 0;

    virtual ~BoundingArea() = default;
    virtual bool In(int x, int y) const = 0;
    virtual bool Intersects(const BoundingArea& area) const = 0;
};

#endif
