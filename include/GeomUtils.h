#ifndef GEOM_UTILS_H
#define GEOM_UTILS_H

#include <array>
#include <cmath>

struct Point2D {
    float x;
    float y;

    Point2D operator+() const;
    Point2D operator-() const;

    Point2D& operator+=(const Point2D& other);
    Point2D& operator-=(const Point2D& other);

    Point2D operator+(const Point2D& other) const;
    Point2D operator-(const Point2D& other) const;
};

struct Rect2D {
    Point2D topLeft;
    Point2D bottomRight;

    Rect2D(const Rect2D& other) = default;
    Rect2D& operator=(const Rect2D& other) = default;
    ~Rect2D() = default;
    Rect2D(const Point2D& _topLeft, const Point2D& _bottomRight) : topLeft{ _topLeft }, bottomRight{ _bottomRight } {}
    Rect2D(const float& _x, const float& _y, const float& _w, const float& _h) : Rect2D{ { _x, _y }, { _x + _w, _y + _h } } {}

    Point2D TL() const { return topLeft; }
    Point2D BR() const { return bottomRight; }
    float x() const { return topLeft.x; }
    float y() const { return topLeft.y; }
    float w() const { return bottomRight.x - topLeft.x; }
    float h() const { return bottomRight.y - topLeft.y; }
};

struct Circle2D {
    Point2D center;
    float radius;

    Circle2D(const Circle2D& other) = default;
    Circle2D& operator=(const Circle2D& other) = default;
    ~Circle2D() = default;
    Circle2D(const Point2D& _center, const float& _radius) : center{ _center }, radius{ _radius } {}
    Circle2D(const float& _x, const float& _y, const float& _radius) : Circle2D{ { _x, _y }, _radius } {}

    Point2D Center() const { return center; }
    float Radius() const { return radius; }
    float x() const { return center.x; }
    float y() const { return center.y; }
};

#endif
