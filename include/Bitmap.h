#ifndef BITMAP_H
#define BITMAP_H

#include "Color.h"

//#include "AllegroFwds.h"

#include <allegro5/allegro5.h>
#include <allegro5/allegro_image.h>

#include <string>

class Bitmap {
private:
    ALLEGRO_BITMAP* bitmap;

public:
    // Bitmap(unsigned x = 0, unsigned y = 0) : bitmap{ al_create_bitmap(x, y), al_destroy_bitmap } {};
    Bitmap(unsigned x = 0, unsigned y = 0) : bitmap{ al_create_bitmap(x, y) } {};
    Bitmap(const std::string& filePath) : bitmap{ al_load_bitmap(filePath.c_str()) } {
        assert(bitmap);
    };
    Bitmap(const ALLEGRO_BITMAP* _bitmap) : bitmap{ al_clone_bitmap(const_cast<ALLEGRO_BITMAP*>(_bitmap)) } {};
    Bitmap(const Bitmap& other) : Bitmap{ other.Get() } {};
    Bitmap operator=(const Bitmap& other) {
        if (this == &other) return *this;
        al_destroy_bitmap(bitmap);
        bitmap = al_clone_bitmap(other.bitmap);
        return *this;
    }
    ~Bitmap() { al_destroy_bitmap(bitmap); };
    const ALLEGRO_BITMAP* Get() const { return bitmap; }
    ALLEGRO_BITMAP* Get() { return bitmap; }
    int GetWidth() const {
        assert(bitmap);
        return al_get_bitmap_width(bitmap);
    };
    int GetHeight() const {
        assert(bitmap);
        return al_get_bitmap_height(bitmap);
    };
    const Color GetPixel(const unsigned& x, const unsigned& y) const {
        assert(bitmap);
        return al_get_pixel(bitmap, x, y);
    }
};
#endif
