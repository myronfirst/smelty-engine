#include "Sprite.h"

#include "BoundingBox.h"
#include "BoundingCircle.h"
#include "BoundingPolygon.h"

#include <allegro5/allegro_primitives.h>
#include <vector>

Sprite::Sprite(const int& x, const int& y, const int& w, const int& h) : boundingType{ 0 }, color{ Color{ 0.0, 0.0, 0.0, 1.0 } }, boundingArea{ new BoundingBox{ x, y, w, h } } {}
Sprite::Sprite(const int& x, const int& y, const float& r) : boundingType{ 1 }, color{ Color{ 0.0, 0.0, 0.0, 1.0 } }, boundingArea{ new BoundingCircle{ x, y, r } } {}
Sprite::Sprite(const std::list<Point2D>& points) : boundingType{ 2 }, color{ Color{ 0.0, 0.0, 0.0, 1.0 } }, boundingArea{ new BoundingPolygon{ points } } {}

void Sprite::RenderBoundingBox() const {
    assert(boundingType == 0);
    const Rect2D box = static_cast<BoundingBox*>(boundingArea)->GetBox();
    al_draw_filled_rectangle(box.TL().x, box.TL().y, box.BR().x, box.BR().y, color.ToAllegroColor());
}

void Sprite::RenderBoundingCircle() const {
    assert(boundingType == 1);
    const Circle2D circle = static_cast<BoundingCircle*>(boundingArea)->GetCircle();
    al_draw_filled_circle(circle.x(), circle.y(), circle.Radius(), color.ToAllegroColor());
}

void Sprite::RenderBoundingPolygon() const {
    assert(boundingType == 2);
    std::vector<float> vertices;
    for (const auto& p : static_cast<BoundingPolygon*>(boundingArea)->GetPoints()) {
        vertices.push_back(p.x);
        vertices.push_back(p.y);
    }
    al_draw_filled_polygon(vertices.data(), static_cast<int>(std::size(vertices)) * sizeof(float), color.ToAllegroColor());
}

void Sprite::SetPositionBox(const int& x, const int& y) {
    assert(boundingType == 0);
    static_cast<BoundingBox*>(boundingArea)->SetPos(x, y);
}

void Sprite::SetPositionCircle(const int& x, const int& y) {
    assert(boundingType == 1);
    static_cast<BoundingCircle*>(boundingArea)->SetPos(x, y);
}

void Sprite::SetPositionPolygon(const int& x, const int& y) {
    assert(boundingType == 2);
    static_cast<BoundingPolygon*>(boundingArea)->SetPos(x, y);
}

void Sprite::SetPosition(const int& x, const int& y) {
    switch (boundingType) {
        case 0: SetPositionBox(x, y); break;
        case 1: SetPositionCircle(x, y); break;
        case 2: SetPositionPolygon(x, y); break;
        default: assert(false); break;
    }
}

void Sprite::RenderBoundingArea() const {
    switch (boundingType) {
        case 0: RenderBoundingBox(); break;
        case 1: RenderBoundingCircle(); break;
        case 2: RenderBoundingPolygon(); break;
        default: assert(false); break;
    }
}

bool Sprite::CollisionCheck(const Sprite* s) const {
    return GetBoundingArea().Intersects(s->GetBoundingArea());
}
