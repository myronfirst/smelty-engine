#ifndef RENDERER_H
#define RENDERER_H

#include "AllegroFwds.h"

#include "Fwds.h"

#include "EventQueue.h"

class Renderer {
private:
    ALLEGRO_DISPLAY_UPTR display;
    EventQueuePtr queue;

public:
    Renderer(unsigned width = 800, unsigned height = 600) : display{ al_create_display(width, height), al_destroy_display }, queue{ nullptr } {
        al_set_new_display_option(ALLEGRO_VSYNC, 1, ALLEGRO_REQUIRE);
    }

    void RegisterQueue(const EventQueuePtr& q) {
        queue = q;
        queue->RegisterSource(al_get_display_event_source(display.get()));
    }

    unsigned Width() const { return al_get_display_width(display.get()); }
    unsigned Height() const { return al_get_display_height(display.get()); }

    void Clear() {
        al_clear_to_color(al_map_rgba_f(0.2f, 0.2f, 0.2f, 1.0f));
    }
    void Text(const ALLEGRO_FONT* font, unsigned x, unsigned y, const char* text) {
        al_draw_text(font, al_map_rgb(255, 255, 255), x, y, 0, text);
    }
    void Flip() {
        al_flip_display();
    }
};
#endif
