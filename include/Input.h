#ifndef INPUT_H
#define INPUT_H

#include <allegro5/allegro5.h>

#include "Fwds.h"

#include "EventQueue.h"

class Input {
private:
    EventQueuePtr queue;

public:
    Input() : queue{ nullptr } {};
    void RegisterQueue(const EventQueuePtr& q) {
        queue = q;
        queue->RegisterSource(al_get_keyboard_event_source());
    }
    const EventQueuePtr& GetQueue() const { return queue; }
};

#endif
