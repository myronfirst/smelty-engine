#include "BitmapLoader.h"

#include <memory>

BitmapPtr BitmapLoader::GetBitmap(const std::string& path) const {
    auto it = bitmaps.find(path);
    return (it != std::cend(bitmaps) ? it->second : nullptr);
}

BitmapPtr BitmapLoader::Load(const std::string& path) {
    BitmapPtr b = GetBitmap(path);
    if (b == nullptr) {
        b = std::make_shared<Bitmap>(path);
        assert(b);
        bitmaps.at(path) = b;
    }
    return b;
}

// prefer to massively clear bitmaps at the end than
// to destroy individual bitmaps during gameplay
void BitmapLoader::CleanUp(void) {
    for (auto& [key, val] : bitmaps)
        val.reset();
    bitmaps.clear();
}