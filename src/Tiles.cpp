#if 0
#include <cstddef>

using std::byte;

#define TILES_ROW (10)
#define TILES_COL (6)
#define TILES_TOTAL (TILES_ROW * TILES_COL)
//10*6 == 60

int main() {
    unsigned char divIndex[TILES_TOTAL];
    unsigned char modIndex[TILES_TOTAL];
    for (unsigned char i = 0; i < TILES_TOTAL; ++i) {
        divIndex[i] = i / TILES_ROW;
        modIndex[i] = i % TILES_ROW;
    }
    unsigned char index = 0;
    unsigned char row = divIndex[index];
    unsigned char column = modIndex[index];
}
/*
#define TILE_WIDTH 16
#define TILE_HEIGHT 16
#define ROW_MASK 0x0F
#define COL_MASK 0xF0
#define COL_SHIFT 4
typedef unsigned char byte;
byte MakeIndex(byte row, byte col) { return (col << COL_SHIFT) | row; }
byte GetCol(byte index) { return index >> COL_SHIFT; }
byte GetRow(byte index) { return index & ROW_MASK; }

Dim TileX(byte index) { return GetCol(index) * TILE_WIDTH; }
Dim TileY(byte index) { return GetRow(index) * TILE_HEIGHT; }

#define MUL_TILE_WIDTH(i) ((i) << 4)
#define MUL_TILE_HEIGHT(i) ((i) << 4)
#define DIV_TILE_WIDTH(i) ((i) >> 4)
#define DIV_TILE_HEIGHT(i) ((i) >> 4)
#define MOD_TILE_WIDTH(i) ((i)&15)
#define MOD_TILE_HEIGHT(i) ((i)&15)

Dim TileX2(byte index) { return MUL_TILE_WIDTH(GetCol(index)); }
Dim TileY2(byte index) { return MUL_TILE_HEIGHT(GetRow(index)); }

typedef unsigned short Index;    // [MSB X][LSB Y]
#define TILEX_MASK 0xFF00
#define TILEX_SHIFT 8
#define TILEY_MASK 0x00FF

Index MakeIndex2(byte row, byte col) { return (MUL_TILE_WIDTH(col) << TILEX_SHIFT) | MUL_TILE_HEIGHT(row); }
Dim TileX3(Index index) { return index >> TILEX_SHIFT; }
Dim TileY3(Index index) { return index & TILEY_MASK; }
void PutTile(Bitmap dest, Dim x, Dim y, Bitmap tiles, Index tile) {
    BitmapBlit(
        tiles, Rect{ TileX3(tile), TileY3(tile), TILE_WIDTH, TILE_HEIGHT },
        dest, Point{ x, y });
}
 */

#endif
