#ifndef BOUNDING_BOX_H
#define BOUNDING_BOX_H

#include "BoundingArea.h"

#include "GeomUtils.h"

class BoundingBox : public BoundingArea {
protected:
    Rect2D box;

public:
    BoundingBox(const Rect2D& _box) : box{ _box } {}
    BoundingBox(const Point2D& _tl, const Point2D& _br) : BoundingBox{ Rect2D{ _tl, _br } } {}
    BoundingBox(const int& _x, const int& _y, const int& _w, const int& _h) : BoundingBox{ { static_cast<float>(_x), static_cast<float>(_y) }, { static_cast<float>(_x) + static_cast<float>(_w), static_cast<float>(_y) + static_cast<float>(_h) } } {}
    bool Intersects(const BoundingBox& _box) const override;
    bool Intersects(const BoundingCircle& _circle) const override;
    bool Intersects(const BoundingPolygon& _poly) const override;

    bool In(int x, int y) const override;
    bool Intersects(const BoundingArea& area) const override { return area.Intersects(*this); };

    const Rect2D& GetBox() const { return box; }
    Point2D GetTL() const { return box.TL(); }
    Point2D GetBR() const { return box.BR(); }

    void SetPos(const int& x, const int& y);
};

#endif
