#ifndef BOUNDING_CIRCLE_H
#define BOUNDING_CIRCLE_H

#include "BoundingArea.h"

#include "GeomUtils.h"

class BoundingCircle : public BoundingArea {
protected:
    Circle2D circle;

public:
    BoundingCircle(const Circle2D& _circle) : circle{ _circle } {}
    BoundingCircle(const Point2D& _c, const float& _r) : BoundingCircle{ { _c, _r } } {}
    BoundingCircle(const int& _x, const int& _y, const float& _r) : BoundingCircle{ { static_cast<float>(_x), static_cast<float>(_y) }, _r } {}
    bool Intersects(const BoundingBox& _box) const override;
    bool Intersects(const BoundingCircle& _circle) const override;
    bool Intersects(const BoundingPolygon& _poly) const override;

    bool In(int x, int y) const override;
    bool Intersects(const BoundingArea& area) const override { return area.Intersects(*this); };

    const Circle2D& GetCircle() const { return circle; }
    Point2D GetCenter() const { return circle.Center(); }
    float GetRadius() const { return circle.Radius(); }
    void SetPos(const int& x, const int& y);
};

#endif
