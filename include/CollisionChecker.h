#ifndef COLLISION_CHECKER_H
#define COLLISION_CHECKER_H

#include <functional>
#include <list>
#include <tuple>
#include "Sprite.h"

class CollisionChecker final {
public:
    using Action = std::function<void(Sprite* s1, Sprite* s2)>;

private:
    using Entry = std::tuple<Sprite*, Sprite*, Action>;
    static CollisionChecker singleton;
    std::list<Entry> entries;

public:
    void Register(Sprite* s1, Sprite* s2, const Action& f);
    void Remove(Sprite* s1, Sprite* s2);
    void Check(void) const;
    static auto Get(void) -> CollisionChecker& { return singleton; }
    static auto GetConst(void) -> const CollisionChecker& { return singleton; }
};

#endif
