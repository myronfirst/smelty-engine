#include "BoundingPolygon.h"

#include <algorithm>

bool BoundingPolygon::Intersects(const BoundingBox& _box) const {
    return false;
}
bool BoundingPolygon::Intersects(const BoundingCircle& _circle) const {
    return false;
}
bool BoundingPolygon::Intersects(const BoundingPolygon& _poly) const {
    return false;
}

bool BoundingPolygon::In(int x, int y) const {
    return false;
}

void BoundingPolygon::SetPos(const int& x, const int& y) {
    const Point2D origin = { points.front().x, points.front().y };
    const Point2D dist = { x - origin.x, y - origin.y };
    std::for_each(std::begin(points), std::end(points), [&dist](Point2D& el) {
        el += dist;
    });
}