#ifndef ALLEGRO_FWDS_H
#define ALLEGRO_FWDS_H

#include <allegro5/allegro5.h>
#include <allegro5/allegro_font.h>

#include <memory>

using ALLEGRO_EVENT_QUEUE_PTR = std::shared_ptr<ALLEGRO_EVENT_QUEUE>;
using ALLEGRO_BITMAP_PTR = std::shared_ptr<ALLEGRO_BITMAP>;
using ALLEGRO_TIMER_PTR = std::shared_ptr<ALLEGRO_TIMER>;
using ALLEGRO_FONT_PTR = std::shared_ptr<ALLEGRO_FONT>;
using ALLEGRO_DISPLAY_PTR = std::shared_ptr<ALLEGRO_DISPLAY>;

using ALLEGRO_EVENT_QUEUE_UPTR = std::unique_ptr<ALLEGRO_EVENT_QUEUE, void (*)(ALLEGRO_EVENT_QUEUE*)>;
using ALLEGRO_BITMAP_UPTR = std::unique_ptr<ALLEGRO_BITMAP, void (*)(ALLEGRO_BITMAP*)>;
using ALLEGRO_TIMER_UPTR = std::unique_ptr<ALLEGRO_TIMER, void (*)(ALLEGRO_TIMER*)>;
using ALLEGRO_FONT_UPTR = std::unique_ptr<ALLEGRO_FONT, void (*)(ALLEGRO_FONT*)>;
using ALLEGRO_DISPLAY_UPTR = std::unique_ptr<ALLEGRO_DISPLAY, void (*)(ALLEGRO_DISPLAY*)>;

#endif
