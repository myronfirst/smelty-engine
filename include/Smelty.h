#ifndef SMELTY_H
#define SMELTY_H

#include "Bitmap.h"
#include "Font.h"
#include "GameLoop.h"
#include "Input.h"
#include "Renderer.h"
#include "Timer.h"

#include "AnimationFilm.h"
#include "GeomUtils.h"

#include "Fwds.h"

#include "EventQueue.h"

#include "AnimationFilmHolder.h"
#include "BitmapLoader.h"
#include "CollisionChecker.h"
#include "Sprite.h"

#include <cstddef>
#include <iostream>
#include <memory>
#include <numeric>
#include <tuple>

constexpr short KEY_SEEN = 1;
constexpr short KEY_RELEASED = 2;

class Smelty {
private:
    EventQueuePtr queue;
    ALLEGRO_EVENT event;

    Renderer renderer;
    Input input;
    Font font;
    Timer timer;
    GameLoop gameLoop;
    bool finished = false;
    bool redraw = false;

    BitmapLoader bitmapLoader;

    //sandbox
    Timer animTimer{ 1.0 / 10.0 };
    AnimationFilm film{ "NoReady" };
    unsigned frameNo;

    double currentTime;
    double lastFrameTime;
    double lastMillisPerFrameTime;
    std::vector<double> elapsedTimes;

    double millisPerFrame;
    double framesPerSecond;

    double lastTime;
    uint64_t frameCounter;
    Timer fpsTimer{ 1.0 / 60.0 };

    int boxX = 30;
    int boxY = 30;

    Sprite box{ boxX, boxY, 60, 60 };
    Sprite box2{ boxX + 100, boxY + 100, 60, 60 };
    Sprite circle{ 100, 100, 30 };
    Sprite polygon{ { Point2D{ 500, 400 }, Point2D{ 500, 500 }, Point2D{ 600, 500 }, Point2D{ 600, 400 } } };

    std::array<short, ALLEGRO_KEY_MAX> keyboard;

    Timer keyboardTimer{ 1.0 / 60.0 };

public:
    Smelty();

    Renderer& GetRenderer() { return renderer; }
    Input& GetInput() { return input; }
    Font& GetFont() { return font; }
    Timer& GetTimer() { return timer; }
    GameLoop& GetGameLoop() { return gameLoop; }

    void PreGameLoopSandbox();
    void Run();

    void EventDispatchCallback();
    void InputCallback();
    void CollisionsCallback();
    void RenderCallback();
    void FPSCallback();
    bool FinishedCallback();

    void RegisterEventSources();
    void SetCallbacks();

    auto ParseEntryCallback(const int& cursorPos, const std::string& text) -> std::tuple<std::string, std::string, std::vector<Rect2D>, int>;
};

#endif