#include "BoundingCircle.h"

bool BoundingCircle::Intersects(const BoundingBox& _box) const {
    return false;
}
bool BoundingCircle::Intersects(const BoundingCircle& _circle) const {
    return false;
}
bool BoundingCircle::Intersects(const BoundingPolygon& _poly) const {
    return false;
}

bool BoundingCircle::In(int x, int y) const {
    return false;
}

void BoundingCircle::SetPos(const int& x, const int& y) {
    circle.center = { static_cast<float>(x), static_cast<float>(y) };
}