﻿#ifndef ANIMATION_H
#define ANIMATION_H

#include <string>

class Animation {
private:
    std::string id;
    int* p;

public:
    Animation(const std::string& _id) : id(_id){};
    auto GetId() const -> const std::string& { return id; };
};
#endif
